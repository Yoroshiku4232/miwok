package com.example.android.miwok;

/**
 * Created by Alelol on 21.03.2017.
 */

public class Word {

    private String defaultTranslation,miwokTranslation;
    private int imageResId;
    private boolean hasImage;

    public boolean isHasImage() {
        return hasImage;
    }

    public int getImageResId() {
        return imageResId;
    }

    public String getMiwokTranslation() {
        return miwokTranslation;
    }

    public void setMiwokTranslation(String miwokTranslation) {
        this.miwokTranslation = miwokTranslation;
    }

    public String getDefaultTranslation() {

        return defaultTranslation;
    }

    public void setDefaultTranslation(String defaultTranslation) {
        this.defaultTranslation = defaultTranslation;
    }

    public Word(String defaultTranslationIn, String miwokTranslationIn) {
        defaultTranslation = defaultTranslationIn;
        miwokTranslation = miwokTranslationIn;
        hasImage = false;
    }

    public Word(String defaultTranslationIn, String miwokTranslationIn, int imageResourceId) {
        defaultTranslation = defaultTranslationIn;
        miwokTranslation = miwokTranslationIn;
        imageResId = imageResourceId;
        hasImage = true;
    }
}
