package com.example.android.miwok;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Alelol on 21.03.2017.
 */

public class WordAdapter extends ArrayAdapter<Word> {

    private int mColorResID;

    public WordAdapter(Activity context, ArrayList<Word> words, int colorResID){
        super(context, 0, words);
        mColorResID = colorResID;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        Word currentWord = getItem(position);

        TextView mivokTextView = (TextView) listItemView.findViewById(R.id.mivok_text_view);
        mivokTextView.setText(currentWord.getMiwokTranslation());

        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.default_text_view);
        defaultTextView.setText(currentWord.getDefaultTranslation());

        ImageView imageView = (ImageView) listItemView.findViewById(R.id.imageMiwok);
        if (currentWord.isHasImage()) {
            imageView.setImageResource(currentWord.getImageResId());
        }else imageView.setVisibility(View.GONE);

        int color = ContextCompat.getColor(getContext(), mColorResID);
        View textContainer = listItemView.findViewById(R.id.text_container);
        textContainer.setBackgroundColor(color);

        return listItemView;
    }
}
